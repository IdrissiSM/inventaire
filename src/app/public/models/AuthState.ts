export interface AuthState {
    isAuthenticated: boolean;
    uid: string;
    userName: string;
    email: string;
    roles: string[];
    token: string;
    expiresOn: Date;
}
