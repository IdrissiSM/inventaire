import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/auth/auth.service';
import { LayoutService } from 'src/app/private/services/app.layout.service';
import { APIResponse } from 'src/app/shared/models/APIResponse';
import { AuthResponse } from 'src/app/shared/models/AuthResponse';
import { LoginCredentials } from 'src/app/shared/models/LoginCredentials';
import { Message } from 'primeng/api';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styles: [
        `
            :host ::ng-deep .pi-eye,
            :host ::ng-deep .pi-eye-slash {
                transform: scale(1.6);
                margin-right: 1rem;
                color: var(--primary-color) !important;
            }
        `,
    ],
})
export class LoginComponent {
    loginForm!: FormGroup;
    loading = false;
    loginFailedMessage = '';
    loginCredentials: LoginCredentials;
    authResponse: AuthResponse;
    messages: Message[] | undefined;

    constructor(
        public layoutService: LayoutService,
        private authService: AuthService,
        private router: Router
    ) {
        this.loginForm = new FormGroup({
            email: new FormControl('', [Validators.required, Validators.email]),
            password: new FormControl('', Validators.required),
            checkbox: new FormControl(false, Validators.required),
        });
    }

    ngOnInit() {
        this.messages = [];
    }

    get email(): string {
        return this.loginForm.get('email')!.value;
    }
    get password(): string {
        return this.loginForm.get('password')!.value;
    }
    get checkbox(): string {
        return this.loginForm.get('checkbox')!.value;
    }

    submit() {
        if (!this.loginForm.valid) return;
        this.loading = true;
        this.loginCredentials = {
            email: this.email,
            password: this.password,
        };
        this.authService
            .login(this.loginCredentials)
            .subscribe(async (response: APIResponse) => {
                this.loading = false;
                console.log(response);
                this.authResponse = await response.result;
                if (response.success) {
                    if (this.authResponse.isAuthenticated) {
                        if (this.checkbox) {
                            this.authService.setAuthStateLocal(
                                this.authResponse.token
                            );
                        } else {
                            this.authService.setAuthStateSession(
                                this.authResponse.token
                            );
                        }
                        this.router.navigate(['']);
                    } else {
                        console.log(this.authResponse.message);
                    }
                } else {
                    const message: Message = {
                        severity: 'error',
                        summary: 'Error',
                        detail: this.authResponse.message,
                    };
                    this.messages.push(message);
                    console.log(response.errorMessages);
                    console.log(this.messages);
                }
            });
    }
}
