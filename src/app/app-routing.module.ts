import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { NotfoundComponent } from 'src/app/shared/components/notfound/notfound.component';
import { AppLayoutComponent } from './private/app.layout.component';
import { AuthGuard } from './core/guards/auth.guard';
import { InverseAuthGuard } from './core/guards/inverse-aut.guard';

const routes: Routes = [
    {
        path: '',
        component: AppLayoutComponent,
        canActivate: [InverseAuthGuard],
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
            // { path: 'dashboard', component: DashboardComponent },
        ],
    },
    {
        path: 'auth',
        canActivate: [AuthGuard],
        loadChildren: () =>
            import('./public/components/auth/auth.module').then(
                (m) => m.AuthModule
            ),
    },
    { path: 'notfound', component: NotfoundComponent },
    { path: '**', redirectTo: '/notfound' },
];
@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
