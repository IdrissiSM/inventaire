import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { LoginCredentials } from 'src/app/shared/models/LoginCredentials';
import { Observable } from 'rxjs';
import { APIResponse } from 'src/app/shared/models/APIResponse';
import { jwtDecode } from 'jwt-decode';
import { AuthState } from 'src/app/public/models/AuthState';

@Injectable({
    providedIn: 'root',
})
export class AuthService {
    apiUrl = environment.apiBaseUrl;
    authState: AuthState = {
        isAuthenticated: false,
        uid: '',
        userName: '',
        email: '',
        roles: [],
        token: '',
        expiresOn: null,
    };

    constructor(private http: HttpClient, private router: Router) {
        this.getAuthState();
    }

    login(loginRequest: LoginCredentials): Observable<APIResponse> {
        return this.http.post<APIResponse>(
            `${this.apiUrl}/auth/login`,
            loginRequest
        );
    }

    getAuthState() {
        const authStateString =
            localStorage.getItem('authState') ??
            sessionStorage.getItem('authState');
        if (authStateString) {
            this.verifyTokenExpiration(authStateString);
        } else {
            this.authState = this.reinitializeAuthState();
        }
    }

    verifyTokenExpiration(authStateString: string) {
        const isExpired: boolean =
            new Date(JSON.parse(authStateString!).expiresOn).getTime() <
            new Date().getTime();
        if (isExpired) {
            this.signOut();
        } else {
            this.authState = JSON.parse(authStateString);
        }
    }

    reinitializeAuthState() {
        return {
            isAuthenticated: false,
            uid: '',
            userName: '',
            email: '',
            roles: [],
            token: '',
            expiresOn: null,
        };
    }

    setAuthStateLocal(token: string) {
        const decodedJWT: any = jwtDecode(token);
        this.authState = {
            isAuthenticated: true,
            userName: decodedJWT.name,
            uid: decodedJWT.uid,
            email: decodedJWT.sub,
            roles: decodedJWT.roles,
            token: token,
            expiresOn: new Date(decodedJWT.exp * 1000),
        };
        localStorage.removeItem('authState');
        localStorage.setItem('authState', JSON.stringify(this.authState));
    }

    setAuthStateSession(token: string) {
        const decodedJWT: any = jwtDecode(token);
        this.authState = {
            isAuthenticated: true,
            userName: decodedJWT.name,
            uid: decodedJWT.uid,
            email: decodedJWT.sub,
            roles: decodedJWT.roles,
            token: token,
            expiresOn: new Date(decodedJWT.exp * 1000),
        };
        sessionStorage.removeItem('authState');
        sessionStorage.setItem('authState', JSON.stringify(this.authState));
    }

    signOut() {
        if (localStorage.getItem('authState')) {
            localStorage.removeItem('authState');
        } else {
            sessionStorage.removeItem('authState');
        }
        this.authState = this.reinitializeAuthState();
        this.router.navigate(['/auth/login']);
    }
}
