import { inject } from '@angular/core';
import {
    ActivatedRouteSnapshot,
    CanActivateFn,
    Router,
    RouterStateSnapshot,
} from '@angular/router';
import { AuthService } from '../auth/auth.service';

export const AuthGuard: CanActivateFn = (
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
) => {
    const isAuthenticated = inject(AuthService).authState.isAuthenticated;
    const router = inject(Router);
    if (isAuthenticated) {
        router.navigate(['/']);
        return false;
    } else {
        return true;
    }
};
